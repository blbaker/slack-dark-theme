# Patterns matching CSS files that should be minified. Files with a -min.css
# suffix will be ignored.
CSS_FILES = $(filter-out %-min.css,$(wildcard \
	css/**/*.css \
	css/**/**/*.css \
))

# Command to run to execute the YUI Compressor.
YUI_COMPRESSOR = java -jar /mnt/d/Sites/slack-dark-theme/node_modules/yuicompressor/build/yuicompressor-2.4.8.jar

# Flags to pass to the YUI Compressor for both CSS and JS.
YUI_COMPRESSOR_FLAGS = --charset utf-8 --verbose

CSS_MINIFIED = $(CSS_FILES:.css=-min.css)

# target: minify - Minifies CSS and JS.
minify: minify-css

# target: minify-css - Minifies CSS.
minify-css: $(CSS_FILES) $(CSS_MINIFIED)

%-min.css: %.css
	@echo '==> Minifying $<'
	$(YUI_COMPRESSOR) $(YUI_COMPRESSOR_FLAGS) --type css $< >$@
	@echo

all: clean build

build: default raw variants rawvariants minify

clean:
	rm -rf css
	mkdir -p css/variants css/raw/variants

default:
	sassc -t compact scss/main.scss css/black.css

variants:
	for sass in scss/themes/build-variants/*--main.scss; do \
		theme=`basename $$sass --main.scss`; \
	sassc -t compact $$sass css/variants/$$theme.css; done

raw:
	sassc -t compact scss/styles.scss css/raw/black.css

rawvariants:
	for sass in scss/themes/build-variants/*--styles.scss; do \
		theme=`basename $$sass --styles.scss`; \
	sassc -t compact $$sass css/raw/variants/$$theme.css; done
