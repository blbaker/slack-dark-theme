const
  path = require('path'),
  fs = require('fs'),
  os = require('os'),
  { execSync } = require('child_process');


const
  isWsl = process.platform === 'linux' && os.release().toLowerCase().includes('microsoft'),
  isWin = process.platform === 'win32' ? true : false,
  isMac = process.platform === 'darwin',
  resourcePaths = [],
  patchedPaths = [],
  color = 'css/raw/variants/palenight-min.css',
  patch =     `
//dark-slack-patch-start
document.addEventListener('DOMContentLoaded', function() {
  // Fetch our CSS in parallel ahead of time
  const cssPath =
    'https://bitbucket.org/blbaker/slack-dark-theme/raw/master/css/raw/variants/palenight-min.css';
  let cssPromise = fetch(cssPath).then((response) => response.text());

  // Insert a style tag into the wrapper view
  cssPromise.then((css) => {
    let s = document.createElement('style');
    s.type = 'text/css';
    s.innerHTML = css;
    document.head.appendChild(s);
  });
});
//dark-slack-patch-end`

if (isWin || isWsl) {
  let appPath;
  if (isWsl) {
    const username = execSync('cmd.exe /c "echo %USERNAME%"').toString().trim();
    appPath = path.join(`/mnt/c/Users/${username}/AppData/Local`, 'slack');
  } else {
    appPath = path.join(process.env.LOCALAPPDATA, 'slack')
  }
  resourcePaths.push(...fs.readdirSync(appPath).sort().filter(x => {
    return x.startsWith('app-') && fs.statSync(path.join(appPath, x)).isDirectory()
  }).map(x => {
    execSync(`npx asar extract ${appPath}/${x}/resources/app.asar ${appPath}/${x}/resources/app.asar.unpacked`);
    return path.join(appPath, x, 'resources/app.asar.unpacked/dist/ssb-interop.bundle.js')
  }))
} else if (isMac) {
  resourcePaths.push('/Applications/Slack.app/Contents/Resources/app.asar.unpacked/dist/ssb-interop.bundle.js')
}

resourcePaths.reverse();


resourcePaths.forEach(x => {
  let contents = fs.readFileSync(x, 'utf-8')
  const matches = contents.match(/\/\/dark-slack-patch-(start|end)/g)
  const hasPatch = contents.includes(patch)
  if (!hasPatch && color !== 'og') {
    fs.appendFileSync(x, patch)
    patchedPaths.push(x)
  }
  if (color === 'og' && matches) {
    let lastIndex = 0
    matches.forEach((x, i) => {
      const index = contents.indexOf(x)
      const isEnd = Boolean(i % 2)
      if (isEnd) {
        contents = contents.substr(0, lastIndex -1) + contents.substr(index + 23);
      }
      lastIndex = index
    })
    fs.writeFileSync(x, contents)
  }

  if (isWin || isWsl) {
    let appPath;
    if (isWsl) {
      const username = execSync('cmd.exe /c "echo %USERNAME%"').toString().trim();
      appPath = path.join(`/mnt/c/Users/${username}/AppData/Local`, 'slack');
    } else {
      appPath = path.join(process.env.LOCALAPPDATA, 'slack')
    }

    fs.readdirSync(appPath).sort().forEach(x => {
      if (x.startsWith('app-') && fs.statSync(path.join(appPath, x)).isDirectory()) {
        execSync(`npx asar pack ${appPath}/${x}/resources/app.asar.unpacked ${appPath}/${x}/resources/app.asar`);
      }
    })
  } else {
    execSync(`npx asar pack /Applications/Slack.app/Contents/Resources/app.asar.unpacked /Applications/Slack.app/Contents/Resources/app.asar`);
  }
})
